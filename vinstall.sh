#!/usr/bin/env bash
set -e

module load python-env
python3 --version
python3 -m venv --system-site-packages phonbias-env
source phonbias-env/bin/activate
pip3 install --upgrade pip
pip3 install --upgrade epitran matplotlib nltk pytorch-nlp requests scikit-learn scipy torch
if [ -d 'flite' ]; then
    pushd flite
    git pull origin master
    popd
else
    git clone https://github.com/festvox/flite.git
fi
pushd flite
./configure && make
cd testsuite
make lex_lookup
mkdir -p ~/bin
cp lex_lookup ~/bin/
popd
