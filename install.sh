#!/usr/bin/env bash
set -e

sudo apt install python3-pip
pip install -U pycodestyle pylint
pip install -U epitran matplotlib nltk pytorch-nlp scikit-learn scipy torch
if [ -d 'flite' ]; then
    pushd flite
    git pull origin master
    popd
else
    git clone https://github.com/festvox/flite.git
fi
cd flite
./configure && make
sudo make install
cd testsuite
make lex_lookup
sudo cp lex_lookup /usr/local/bin
