#!/usr/bin/env bash
#SBATCH --account=x
#SBATCH --job-name=PhonBias
#SBATCH --mail-type=ALL
#SBATCH --mail-user=x
#SBATCH --mem=500g
#SBATCH --partition=hugemem
#SBATCH --time=72:00:00
#SBATCH -o submit_nld-Latn.out

source vrun.sh nld-Latn
