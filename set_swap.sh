#!/usr/bin/env bash
set -e

# Turn swap off
# This moves stuff in swap to the main memory and might take several minutes
swapoff -a

# Create an empty swapfile
# Note that "1G" is basically just the unit and count is an integer.
# Together, they define the size. In this case 128 GB.
dd if=/dev/zero of=/swapfile bs=1G count=128
# Set the correct permissions
chmod 0600 /swapfile

mkswap /swapfile  # Set up a Linux swap area
swapon /swapfile  # Turn the swap on
grep -i swaptotal /proc/meminfo
free -m
