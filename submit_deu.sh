#!/usr/bin/env bash
#SBATCH --account=x
#SBATCH --job-name=PhonBias
#SBATCH --mail-type=ALL
#SBATCH --mail-user=x
#SBATCH --mem=500g
#SBATCH --partition=hugemem_longrun
#SBATCH --time=96:00:00
#SBATCH -o submit_deu-Latn.out

source vrun.sh deu-Latn
