#!/usr/bin/env bash
set -e

rm -fv figs/*.png
python3 phonbias.py 10000 eng-Latn >run.out
