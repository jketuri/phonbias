#!/usr/bin/env python
# coding: utf-8

""" Phonological Bias """

from functools import reduce
from os import makedirs
from os.path import join
from sys import argv, stderr
from time import time
from typing import Callable, List, Set, Tuple

from epitran.vector import VectorsWithIPASpace
import matplotlib.pyplot as plt
from nltk.stem.snowball import SnowballStemmer
import numpy as np
from scipy.spatial.distance import cdist
from scipy.stats import pearsonr
from sklearn.cluster import AffinityPropagation
from sklearn.decomposition import PCA
from sklearn.metrics import adjusted_rand_score
import torch
from torchnlp.word_to_vector import FastText


CONVERGENCE_ITER = 15
DAMPING = 0.9
FIG_DIR = 'figs'
MAX_ITER = 2000
STEP = 0.1
TRIES = 1
WORD_COUNT = 10000

LANGUAGES = [
    ('aar-Latn', 'Afar', 'aa'),
    ('amh-Ethi', 'Amharic', 'am', None, True),
    ('ara-Arab', 'Literary Arabic', 'ar', 'arabic'),
    ('aze-Cyrl', 'Azerbaijani (Cyrillic),', 'az'),
    ('aze-Latn', 'Azerbaijani (Latin),', 'az'),
    ('ben-Beng', 'Bengali', 'bn'),
    ('ben-Beng-red', 'Bengali (reduced),', 'bn'),
    ('cat-Latn', 'Catalan', 'ca'),
    ('ceb-Latn', 'Cebuano', 'ceb'),
    ('cmn-Hans', 'Mandarin (Simplified),*', 'zh'),
    ('cmn-Hant', 'Mandarin (Traditional),*', 'zh'),
    ('ckb-Arab', 'Sorani', 'ar'),
    ('deu-Latn', 'German', 'de', 'german', True),
    ('deu-Latn-np', 'German', 'de', 'german'),
    ('deu-Latn-nar', 'German (more phonetic),', 'de', 'german'),
    ('eng-Latn', 'English', 'en', 'english', True),
    ('fas-Arab', 'Farsi (Perso-Arabic),', 'fa'),
    ('fra-Latn', 'French', 'fr', 'french'),
    ('fra-Latn-np', 'French', 'fr', 'french'),
    ('hau-Latn', 'Hausa', 'ha'),
    ('hin-Deva', 'Hindi', 'hi'),
    ('hun-Latn', 'Hungarian', 'hu', 'hungarian'),
    ('ilo-Latn', 'Ilocano', 'ilo'),
    ('ind-Latn', 'Indonesian', 'id'),
    ('ita-Latn', 'Italian', 'it', 'italian'),
    ('jav-Latn', 'Javanese', 'jv'),
    ('kaz-Cyrl', 'Kazakh (Cyrillic),', 'kk'),
    ('kaz-Latn', 'Kazakh (Latin),', 'kk'),
    ('kin-Latn', 'Kinyarwanda', 'rw'),
    ('kir-Arab', 'Kyrgyz (Perso-Arabic),', 'ky'),
    ('kir-Cyrl', 'Kyrgyz (Cyrillic),', 'ky'),
    ('kir-Latn', 'Kyrgyz (Latin),', 'ky'),
    ('kmr-Latn', 'Kurmanji', 'ku'),
    ('lao-Laoo', 'Lao', 'lo'),
    ('mar-Deva', 'Marathi', 'mr'),
    ('mlt-Latn', 'Maltese', 'mt'),
    ('mya-Mymr', 'Burmese', 'my'),
    ('msa-Latn', 'Malay', 'ms'),
    ('nld-Latn', 'Dutch', 'nl', 'dutch', True),
    ('nya-Latn', 'Chichewa', 'ny'),
    ('orm-Latn', 'Oromo', 'om'),
    ('pan-Guru', 'Punjabi (Eastern),', 'pa'),
    ('pol-Latn', 'Polish', 'pl'),
    ('por-Latn', 'Portuguese', 'pt', 'portuguese'),
    ('ron-Latn', 'Romanian', 'ro', 'romanian'),
    ('rus-Cyrl', 'Russian', 'ru', 'russian'),
    ('sna-Latn', 'Shona', 'sn'),
    ('som-Latn', 'Somali', 'so'),
    ('spa-Latn', 'Spanish', 'es', 'spanish', True),
    ('swa-Latn', 'Swahili', 'sw'),
    ('swe-Latn', 'Swedish', 'sv', 'swedish'),
    ('tam-Taml', 'Tamil', 'ta'),
    ('tel-Telu', 'Telugu', 'te'),
    ('tgk-Cyrl', 'Tajik', 'tg'),
    ('tgl-Latn', 'Tagalog', 'tl'),
    ('tha-Thai', 'Thai', 'th'),
    ('tir-Ethi', 'Tigrinya', 'ti', None, True),
    ('tpi-Latn', 'Tok Pisin', 'tpi'),
    ('tuk-Cyrl', 'Turkmen (Cyrillic),', 'tk'),
    ('tuk-Latn', 'Turkmen (Latin),', 'tk'),
    ('tur-Latn', 'Turkish (Latin),', 'tr', None, True),
    ('ukr-Cyrl', 'Ukranian', 'uk'),
    ('uig-Arab', 'Uyghur (Perso-Arabic),', 'ug'),
    ('uzb-Cyrl', 'Uzbek (Cyrillic),', 'uz'),
    ('uzb-Latn', 'Uzbek (Latin),', 'uz', None, True),
    ('vie-Latn', 'Vietnamese', 'vi'),
    ('xho-Latn', 'Xhosa', 'xh'),
    ('yor-Latn', 'Yoruba', 'yo'),
    ('zul-Latn', 'Zulu', 'zu')
]


def measure_time(
        name: str,
        method: Callable
) -> object:
    """ measure time spent in a method """
    start_time = time()
    result = method()
    end_time = time()
    print(name, 'took', round((end_time - start_time) / 60.0, 2), 'minutes')
    return result


# pylint: disable=too-many-arguments
def get_subset_and_plot(
        x_set: torch.tensor,
        dist_cluster_index,
        dist_cluster_ids_x,
        title: str,
        no_plot: bool = False,
        no_fig: bool = False
) -> Tuple[float, np.ndarray, int]:
    """ get subset of data and plot a scatter diagram """
    # pylint: disable=no-member
    x_subset = torch.index_select(
        x_set, 0,
        torch.tensor([index for index in range(len(x_set))
                      if dist_cluster_ids_x[index] == dist_cluster_index]))
    if len(x_subset) < 2:
        return 0.0, None, len(x_subset)
    x_subset = x_subset.detach()
    dist = cdist(x_subset, x_subset, metric='cosine').flatten()
    nstd = np.nanstd(dist)
    print()
    print(f'{title} nstd = {nstd}, cluster size={len(x_subset)}')
    if not no_fig:
        pca = PCA(n_components=2)
        coords = pca.fit_transform(x_subset)
        plt.figure(figsize=(4, 3), dpi=160)
        plt.scatter(coords[:, 0], coords[:, 1])
        plt.title(title)
        if no_plot:
            plt.savefig(join(FIG_DIR, title))
            plt.close()
        else:
            plt.show()
    else:
        coords = None
    return nstd, coords, len(x_subset)


def show_vocab_subset(  # pylint: disable=too-many-arguments,too-many-locals
        dist_cluster_index: int,
        dist_cluster_ids_x: List,
        dist_weight_subset: List,
        nstd_sum_dist: int,
        nstd_sum_phon: int,
        nstd_delta: int,
        total_count: int,
        vocab: List,
        indices: List,
        tokens_subset: List,
        title: str,
        no_plot: bool = False,
        no_fig: bool = False
) -> Tuple[np.ndarray, np.ndarray, float, float, float, int, float, float]:
    """ show subset of vocabulary as semantic and phonetic diagrams """
    vocab_subset = [
        vocab[indices[index]].strip('#')
        for index in range(len(dist_cluster_ids_x))
        if dist_cluster_ids_x[index] == dist_cluster_index]
    print(f'{title} dist words {dist_cluster_index + 1}')
    print(vocab_subset)
    nstd_dist, coords_dist, dist_cluster_size = get_subset_and_plot(
        dist_weight_subset, dist_cluster_index, dist_cluster_ids_x,
        f'{title} dist cluster {dist_cluster_index + 1}', no_plot, no_fig)
    nstd_phon, coords_phon, phon_cluster_size = get_subset_and_plot(
        tokens_subset, dist_cluster_index, dist_cluster_ids_x,
        f'{title} dist cluster {dist_cluster_index + 1} phon', no_plot, no_fig)
    # pylint: disable=comparison-with-itself
    if (nstd_dist is not None and nstd_phon is not None and
            nstd_dist == nstd_dist and nstd_phon == nstd_phon):
        nstd_sum_dist += nstd_dist
        nstd_sum_phon += nstd_phon
        nstd_delta += nstd_phon - nstd_dist
        total_count += 1
    print()
    print()
    return (coords_dist, coords_phon, nstd_sum_dist, nstd_sum_phon,
            nstd_delta, total_count, nstd_dist, nstd_phon,
            dist_cluster_size, phon_cluster_size)


def print_results(
        # pylint: disable=too-many-arguments, too-many-locals, unused-argument
        title: str,
        total_count: int,
        nstd_sum_dist: float,
        nstd_sum_phon: float,
        nstd_delta: float,
        vocab_size: int,
        damping: float,
        convergence_iter: int,
        dist_phon_cov: float,
        dist_phon_cor: float,
        dist_dist_size_cov: float,
        dist_dist_size_cor: float,
        phon_phon_size_cov: float,
        phon_phon_size_cor: float,
        dist_phon_size_cov: float,
        dist_phon_size_cor: float,
        phon_dist_size_cov: float,
        phon_dist_size_cor: float,
        dist_cluster_sizes: List[int],
        dist_cluster_ids_x: np.ndarray,
        rand_score: float
) -> Tuple[float, float]:
    """ print results in text format """
    print()
    print(f'# {title} & \\\\')
    print(f'# nstd total count & {total_count} \\\\')
    print(f'# nstd sum dist & {nstd_sum_dist} \\\\')
    print(f'# nstd sum phon & {nstd_sum_phon} \\\\')
    nstd_mean_dist = float(nstd_sum_dist) / float(total_count)
    print(f'# nstd mean dist & {nstd_mean_dist} \\\\')
    nstd_mean_phon = float(nstd_sum_phon) / float(total_count)
    print(f'# nstd mean phon & {nstd_mean_phon} \\\\')
    print(f'# nstd delta & {nstd_delta} \\\\')
    print(f'# vocabulary size & {vocab_size} \\\\')
    print(f'# damping & {damping} \\\\')
    print(f'# convergence iter & {convergence_iter} \\\\')
    print(f'# dist phon cov & {dist_phon_cov} \\\\')
    print(f'# dist phon cor & {dist_phon_cor} \\\\')
    print(f'# dist dist size cov & {dist_dist_size_cov} \\\\')
    print(f'# dist dist size cor & {dist_dist_size_cor} \\\\')
    print(f'# phon phon size cov & {phon_phon_size_cov} \\\\')
    print(f'# phon phon size cor & {phon_phon_size_cor} \\\\')
    print(f'# dist phon size cov & {dist_phon_size_cov} \\\\')
    print(f'# dist phon size cor & {dist_phon_size_cor} \\\\')
    print(f'# phon dist size cov & {phon_dist_size_cov} \\\\')
    print(f'# phon dist size cor & {phon_dist_size_cor} \\\\')
    if rand_score is not None:
        print(f'# rand score & {rand_score} \\\\')
    print()
    return nstd_mean_dist, nstd_mean_phon


def show_vocab_subsets(
        # pylint: disable=too-many-arguments,too-many-locals
        dist_weight_subset: np.array,
        dist_cluster_ids_x: List,
        vocab: List,
        indices: List,
        tokens_subset: List,
        title: str,
        no_plot: bool = False,
        no_fig: bool = False
) -> Tuple[
        int, float, float, float, int, List[Tuple[np.ndarray, float]],
        float, float, float, float, float, float, float, float, float, float,
        List[int]]:
    """ show vocabulary subsets in semantic and phonetic diagrams """
    total_count = 0
    nstd_delta = 0
    nstd_sum_dist = 0.0
    nstd_sum_phon = 0.0
    graph_data = []
    nstd_dist_values = []
    nstd_phon_values = []
    dist_cluster_sizes = []
    phon_cluster_sizes = []
    for dist_cluster_index in range(0, max(dist_cluster_ids_x) + 1):
        (coords_dist, _, nstd_sum_dist, nstd_sum_phon,
         nstd_delta, total_count, nstd_dist, nstd_phon,
         dist_cluster_size, phon_cluster_size) = show_vocab_subset(
            dist_cluster_index, dist_cluster_ids_x, dist_weight_subset,
             nstd_sum_dist, nstd_sum_phon, nstd_delta,
             total_count, vocab, indices, tokens_subset, title,
             no_plot, no_fig)
        # pylint: disable=comparison-with-itself
        if (nstd_dist is not None and nstd_phon is not None and
                nstd_dist == nstd_dist and nstd_phon == nstd_phon):
            nstd_dist_values.append(nstd_dist)
            nstd_phon_values.append(nstd_phon)
            dist_cluster_sizes.append(dist_cluster_size)
            phon_cluster_sizes.append(phon_cluster_size)
            if not no_fig and coords_dist is not None:
                graph_data.append((coords_dist, nstd_phon))
    if not total_count:
        total_count = 1
    dist_phon_cov = None
    dist_phon_cor = None
    dist_dist_size_cov = None
    dist_dist_size_cor = None
    phon_phon_size_cov = None
    phon_phon_size_cor = None
    dist_phon_size_cov = None
    dist_phon_size_cor = None
    phon_dist_size_cov = None
    phon_dist_size_cor = None
    # pylint: disable=broad-except
    try:
        dist_phon_cov = np.cov(nstd_dist_values, nstd_phon_values)
        dist_phon_cor = pearsonr(nstd_dist_values, nstd_phon_values)
        dist_dist_size_cov = np.cov(nstd_dist_values, dist_cluster_sizes)
        dist_dist_size_cor = pearsonr(nstd_dist_values, dist_cluster_sizes)
        phon_phon_size_cov = np.cov(nstd_phon_values, phon_cluster_sizes)
        phon_phon_size_cor = pearsonr(nstd_phon_values, phon_cluster_sizes)
        dist_phon_size_cov = np.cov(nstd_dist_values, phon_cluster_sizes)
        dist_phon_size_cor = pearsonr(nstd_dist_values, phon_cluster_sizes)
        phon_dist_size_cov = np.cov(nstd_phon_values, dist_cluster_sizes)
        phon_dist_size_cor = pearsonr(nstd_phon_values, dist_cluster_sizes)
    except Exception as ex:
        print('!! Invalid values')
        print(ex)
    return (total_count, nstd_sum_dist, nstd_sum_phon,
            nstd_delta / total_count,
            len(vocab), graph_data, dist_phon_cov, dist_phon_cor,
            dist_dist_size_cov, dist_dist_size_cor, phon_phon_size_cov,
            phon_phon_size_cor, dist_phon_size_cov, dist_phon_size_cor,
            phon_dist_size_cov, phon_dist_size_cor, dist_cluster_sizes)


def add_to_set(
        stemmer: SnowballStemmer,
        word_set: Set,
        word: str
) -> bool:
    """ add to a set if not present and return a flag
    indicating the presense """
    word = stemmer.stem(word)
    if word in word_set:
        return False
    word_set.add(word)
    return True


def _word_to_segs(
        vwis: VectorsWithIPASpace,
        word: str
) -> List:
    # pylint: disable=broad-except
    try:
        return vwis.word_to_segs(word)
    except Exception as ex:
        print('missing word:', word)
        print(ex)
        return [[]]


def cluster_and_plot(
        # pylint: disable=too-many-arguments,too-many-branches,too-many-locals
        # pylint: disable=too-many-nested-blocks,too-many-statements
        language: str,
        language_name: str,
        language_abbr: str,
        stemmer_name: str = None,
        use_phones: bool = False,
        use_stemmer: bool = False,
        test: bool = False,
        phon_clusters: bool = False,
        random_clusters: bool = False,
        title_suffix: str = '',
        total_count: int = None,
        no_plot: bool = False,
        no_fig: bool = False,
        damping: float = DAMPING,
        convergence_iter: int = CONVERGENCE_ITER,
        word_count: int = WORD_COUNT,
        dist_cluster_sizes: List[int] = None,
        dist_cluster_ids_x: np.ndarray = None,
        dist_cluster_sizes1: List[int] = None,
        dist_cluster_ids_x1: np.ndarray = None
) -> Tuple[int, float, float, float, int, float, int, float, float, float,
           float, float, float, float, float, float, float, List[int],
           np.ndarray, float]:
    """ cluster semantic vectors and plot diagrams """
    if use_stemmer:
        stemmer = SnowballStemmer(stemmer_name)
        stems = set()
    words = FastText(language=language_abbr)
    if word_count:
        vocab = words.index_to_token[:word_count]
        dist_weight = words.vectors[:word_count]
    else:
        vocab = words.index_to_token
        dist_weight = words.vectors
    print(f'# embedding count & {len(words.index_to_token)} \\\\')
    vwis = VectorsWithIPASpace(language, [language])
    indices = [index for index in range(len(dist_weight))
               if vocab[index].isalpha() and (
                       not use_stemmer or add_to_set(
                           stemmer, stems, vocab[index]))]
    # pylint: disable=no-member
    dist_weight_subset = torch.index_select(
        dist_weight, 0, torch.tensor(indices))
    charmap = {char: index + 1 for index, char in enumerate(
        {letter for token in vocab for letter in token})}
    tokens_subset = [None] * len(indices)
    for index in range(0, len(indices)):
        tokens_subset[index] = [
            charmap[char] for char in vocab[indices[index]]]
    max_len = len(max(tokens_subset, key=len))
    tokens_subset = torch.tensor(list(map(  # pylint: disable=no-member
        lambda token: token + ([0] * (max_len - len(token))), tokens_subset)))
    if use_phones:
        tokens_subset = [reduce((
            lambda phon_vec1, phon_vec2: phon_vec1 + phon_vec2),
                                [(seg[5] if isinstance(seg[5], list)
                                 else list(seg[5])) if seg else [] for seg in
                                 _word_to_segs(vwis, vocab[index])])
                         for index in indices]
        max_len = len(max(tokens_subset, key=len))
        tokens_subset = torch.tensor(list(map(  # pylint: disable=no-member
            lambda token: token + ([0] * (max_len - len(token))),
            tokens_subset)))
    print(f'# token count & {len(tokens_subset)} \\\\')
    if phon_clusters:
        x_subset = tokens_subset.detach()
    else:
        x_subset = dist_weight_subset.detach()
    if not test:
        dist_cluster_ids_x = measure_time(
            'clustering', lambda: AffinityPropagation(
                damping=damping, max_iter=MAX_ITER,
                convergence_iter=convergence_iter,
                random_state=None).fit_predict(X=x_subset))
    if phon_clusters:
        title = (
            f'{language_name} phon clusters conv iter {convergence_iter} '
            f'test{title_suffix}')
    elif test:
        if random_clusters:
            dist_cluster_ids_x = np.random.default_rng().permutation(
                dist_cluster_ids_x).tolist()
        else:
            dist_cluster_sizes0 = dist_cluster_sizes1.copy()
            dist_cluster_ids_x0 = dist_cluster_ids_x1.tolist()
            dist_cluster_ids_x_set = [False] * len(dist_cluster_ids_x1)
            cluster_index0 = 0
            for cluster_index in range(total_count):
                for index, _ in enumerate(dist_cluster_ids_x):
                    if (dist_cluster_ids_x[index] == cluster_index and
                            not dist_cluster_ids_x_set[index]):
                        while (dist_cluster_sizes0[cluster_index0] == 0 and
                               cluster_index0 < len(dist_cluster_sizes0) - 1):
                            cluster_index0 += 1
                        dist_cluster_ids_x0[index] = cluster_index0
                        dist_cluster_ids_x_set[index] = True
                        dist_cluster_sizes0[cluster_index0] -= 1
            dist_cluster_ids_x = dist_cluster_ids_x0
            dist_cluster_sizes = dist_cluster_sizes0
            total_count = len(dist_cluster_sizes)
        title = (
            f'{language_name} test clusters conv iter {convergence_iter} '
            f'test{title_suffix}')
    else:
        title = (
            f'{language_name} dist clusters conv iter {convergence_iter}'
            f'{title_suffix}')
    if phon_clusters or test:
        rand_score = adjusted_rand_score(
            dist_cluster_ids_x, dist_cluster_ids_x1)
    else:
        rand_score = adjusted_rand_score(
            dist_cluster_ids_x, dist_cluster_ids_x)
    n_clusters = len(np.unique(dist_cluster_ids_x))
    print(f'# number of estimated clusters & {n_clusters} \\\\')
    (total_count, nstd_sum_dist, nstd_sum_phon, nstd_delta,
     vocab_size, graph_data, dist_phon_cov,
     dist_phon_cor, dist_dist_size_cov, dist_dist_size_cor, phon_phon_size_cov,
     phon_phon_size_cor, dist_phon_size_cov, dist_phon_size_cor,
     phon_dist_size_cov, phon_dist_size_cor,
     dist_cluster_sizes) = measure_time(
         'subsets', lambda: show_vocab_subsets(
             dist_weight_subset, dist_cluster_ids_x, vocab, indices,
             tokens_subset, title, no_plot, no_fig))
    if not no_fig:
        draw_picture(graph_data, f'{title} summary', no_plot)
    return (
        total_count, nstd_sum_dist, nstd_sum_phon, nstd_delta,
        vocab_size, damping, convergence_iter, dist_phon_cov, dist_phon_cor,
        dist_dist_size_cov, dist_dist_size_cor, phon_phon_size_cov,
        phon_phon_size_cor, dist_phon_size_cov, dist_phon_size_cor,
        phon_dist_size_cov, phon_dist_size_cor, dist_cluster_sizes,
        dist_cluster_ids_x, rand_score)


def draw_picture(  # pylint: disable=too-many-locals
        graph_data: List[Tuple[np.ndarray, float]],
        title: str,
        no_plot: bool = False
):
    """ draw a 3D picture """
    min_x = min([min(graph_datum[0][:, 0]) for graph_datum in graph_data])
    max_x = max([max(graph_datum[0][:, 0]) for graph_datum in graph_data])
    min_y = min([min(graph_datum[0][:, 1]) for graph_datum in graph_data])
    max_y = max([max(graph_datum[0][:, 1]) for graph_datum in graph_data])
    min_z = min([graph_datum[1] for graph_datum in graph_data])
    max_z = max([graph_datum[1] for graph_datum in graph_data])
    print(
        'min_x=', min_x, ', max_x=', max_x,
        ', min_y=', min_y, ', max_y=', max_y,
        ', min_z=', min_z, ', max_z=', max_z,
        file=stderr)
    x_set = np.arange(start=min_x, stop=max_x, step=STEP)
    y_set = np.arange(start=min_y, stop=max_y, step=STEP)
    z_set = np.zeros((len(y_set), len(x_set)))
    x_set, y_set = np.meshgrid(x_set, y_set)
    for graph_datum in graph_data:
        for dist_coord in graph_datum[0]:
            z_set[int((dist_coord[1] - min_y) / STEP),
                  int((dist_coord[0] - min_x) / STEP)] = graph_datum[1]
    fig = plt.figure(figsize=(20, 20))
    ax_gca = fig.gca(projection='3d')
    ax_gca.set_title(title)
    ax_gca.set_zlim(0, max_z)
    ax_gca.set_xlabel('embedding-1')
    ax_gca.set_ylabel('embedding-2')
    ax_gca.set_zlabel('phonetic_deviation')
    ax_gca.plot_surface(X=x_set, Y=y_set, Z=z_set)
    if no_plot:
        plt.savefig(join(FIG_DIR, title))
        plt.close()
    else:
        plt.show()


def show_language_results(
        # pylint: disable=too-many-branches,too-many-function-args
        # pylint: disable=too-many-locals,too-many-statements
        language: Tuple,
        damping: float,
        convergence_iter: int,
        word_count: int
):
    """ show language results """
    print(
        'language=', language, ', damping=', damping,
        ', convergence_iter=', convergence_iter,
        file=stderr)
    makedirs(FIG_DIR, exist_ok=True)
    no_plot = True
    no_test = False

    dist_clusters_result = cluster_and_plot(
        language[0], language[1], language[2],
        use_phones=True, test=False, phon_clusters=False, no_plot=no_plot,
        damping=damping, convergence_iter=convergence_iter,
        word_count=word_count)
    dist_clusters_nstd_mean_dist, dist_clusters_nstd_mean_phon = print_results(
        'dist clusters', *dist_clusters_result)

    if no_test:
        return

    phon_clusters_result = cluster_and_plot(
        language[0], language[1], language[2],
        use_phones=True, test=False, phon_clusters=True, no_plot=no_plot,
        damping=damping, convergence_iter=convergence_iter,
        word_count=word_count,
        dist_cluster_sizes1=dist_clusters_result[-3],
        dist_cluster_ids_x1=dist_clusters_result[-2])
    (phon_clusters_nstd_mean_dist,
     phon_clusters_nstd_mean_phon) = print_results(
         'phon clusters', *phon_clusters_result)

    test_results = []
    test_results.append(cluster_and_plot(
        language[0], language[1], language[2],
        use_phones=True, test=True, phon_clusters=False,
        title_suffix='',
        total_count=phon_clusters_result[0], no_plot=no_plot,
        convergence_iter=convergence_iter, word_count=word_count,
        dist_cluster_sizes=phon_clusters_result[-3],
        dist_cluster_ids_x=phon_clusters_result[-2],
        dist_cluster_sizes1=dist_clusters_result[-3],
        dist_cluster_ids_x1=dist_clusters_result[-2]))
    for tries in range(TRIES):
        test_results.append(cluster_and_plot(
            language[0], language[1], language[2],
            use_phones=True, test=True, phon_clusters=False,
            random_clusters=True,
            title_suffix=f' {tries + 1}',
            total_count=dist_clusters_result[0],
            no_plot=True, no_fig=True,
            convergence_iter=convergence_iter, word_count=word_count,
            dist_cluster_sizes=dist_clusters_result[-3],
            dist_cluster_ids_x=dist_clusters_result[-2],
            dist_cluster_sizes1=phon_clusters_result[-3],
            dist_cluster_ids_x1=phon_clusters_result[-2]))

    if dist_clusters_nstd_mean_phon > dist_clusters_nstd_mean_dist:
        print('dist clusters nstd mean phone greater than dist')
        print('dist clusters test succeed')
    else:
        print('dist clusters test failed')

    if phon_clusters_nstd_mean_phon < phon_clusters_nstd_mean_dist:
        print('phon clusters nstd mean phone less than dist')
        print('phon clusters test succeed')
    else:
        print('phon clusters test failed')

    failed = False
    number = 1
    nstd_mean_phon_more_count = 0
    nstd_mean_phon_more_test_count = 0
    nstd_mean_dist_more_test_count = 0
    for test_result in test_results:
        print()
        print('try', number)
        test_nstd_mean_dist, test_nstd_mean_phon = print_results(
            'test', *test_result)
        if test_nstd_mean_phon > test_nstd_mean_dist:
            nstd_mean_phon_more_count += 1
        if test_nstd_mean_dist > dist_clusters_nstd_mean_dist:
            nstd_mean_dist_more_test_count += 1
        if test_nstd_mean_phon > dist_clusters_nstd_mean_phon:
            if number == 1:
                failed = True
            nstd_mean_phon_more_test_count += 1
        number += 1
    print(
        'count of mean phone greater with tests=',
        nstd_mean_phon_more_count)
    print(
        'count of test mean dist greater with tests=',
        nstd_mean_dist_more_test_count)
    print(
        'count of test mean phone greater with tests=',
        nstd_mean_phon_more_test_count)

    if nstd_mean_phon_more_count != 0:
        print('repeat test 1 failed')
    else:
        print('repeat test 1 succeed')

    if nstd_mean_dist_more_test_count != TRIES + 1:
        print('repeat test 2 failed')
    else:
        print('repeat test 2 succeed')

    if nstd_mean_phon_more_test_count != TRIES or failed:
        print('repeat test 3 failed')
    else:
        print('repeat test 3 succeed')


def show_convergence_results(
        language: str,
        damping: float,
        word_count: int
):
    """ show results with increasing convergence iter parameter """
    for convergence_iter in (1, CONVERGENCE_ITER):
        show_language_results(language, damping, convergence_iter, word_count)


def show_results(
        damping: float,
        word_count: int
):
    """ show results for all languages """
    for language in LANGUAGES:
        if len(language) == 5 and language[4]:
            show_convergence_results(language, damping, word_count)


def show_language_result(
        language_code: str,
        damping: float = DAMPING,
        convergence_iter: int = None,
        word_count: int = WORD_COUNT
):
    """ show language result """
    language = list(filter(
        lambda language: language[0] == language_code, LANGUAGES))[0]
    if convergence_iter is not None:
        show_language_results(
            language, damping, convergence_iter, word_count)
    else:
        show_convergence_results(language, damping, word_count)


show_language_result(
    argv[2], convergence_iter=CONVERGENCE_ITER, word_count=int(argv[1]))
