#!/usr/bin/env bash
set -e

source phonbias-env/bin/activate
WORK_DIR=/scratch/x/x/$1
mkdir -p $WORK_DIR
rm -fr $WORK_DIR/figs
cd $WORK_DIR
python3 ~/phonbias/phonbias.py 100000 $1
rm -f ~/figs_$1.zip ~/submit_$1.zip
zip -r ~/figs_$1.zip figs
cd ~/phonbias
zip ~/submit_$1.zip submit_$1.out
